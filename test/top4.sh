echo "Setting up peerster..."
./peerster -UIPort 8080 -GUIPort 8180 -gossipAddr 127.0.0.1:5000 -name A -antiEntropy 5 -rtimer 5 -N 2 -peers 127.0.0.1:5001 &
./peerster -UIPort 8081 -GUIPort 8181 -gossipAddr 127.0.0.1:5001 -name B -antiEntropy 5 -rtimer 5 -N 2 -peers 127.0.0.1:5000

echo "Waiting..."
sleep 3

echo "Indexing files..."
./client/client -UIPort 8080 -file file1.txt
sleep 1
./client/client -UIPort 8081 -file file2.txt

echo "Files indexed..."

sleep 99999
