package main

import (
	"encoding/hex"
	"flag"
	"fmt"
	"github.com/CedMaire/peerster/src/defaults"
	"github.com/CedMaire/peerster/src/structs/client"
	"math"
	"net"
	"os"
	"strings"
)

func buildClient(name, uiPort, message, destination, file, request string, budget int, keywords []string) client.Client {
	clientAddress, err := net.ResolveUDPAddr(defaults.UDP4, defaults.LocalHost+":"+uiPort)
	if err != nil {
		panic(err)
	}
	clientConnection, err := net.ListenUDP(defaults.UDP4, &net.UDPAddr{Port: 0})
	if err != nil {
		panic(err)
	}

	var requestBytes []byte
	if request != defaults.FileRequest {
		requestBytes, err = hex.DecodeString(request)
		if err != nil {
			panic(err)
		}
	}

	if len(keywords) > 0 && keywords[0] == defaults.FileSearchKeywords {
		keywords = []string{}
	}

	return client.Client{
		Name:             name,
		ClientAddress:    *clientAddress,
		ClientConnection: *clientConnection,
		Message:          message,
		Destination:      destination,
		File:             file,
		Request:          requestBytes,
		Budget:           uint64(math.Max(math.Min(defaults.FileSearchBudgetMax, float64(budget)), defaults.FileSearchBudgetMin)),
		Keywords:         keywords,
	}
}

func main() {
	hostname, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	hostname = hostname + "-client-groot"

	var (
		uiPort      = flag.String("UIPort", defaults.GossiperClientUIPort, "port for the ui client")
		message     = flag.String("msg", defaults.ClientMessage, "message to send")
		destination = flag.String("dest", defaults.ClientDestination, "destination of the message, defaults to broadcast")
		file        = flag.String("file", defaults.FilePath, "path of a file")
		request     = flag.String("request", defaults.FileRequest, "metahash of the file to download")
		budget      = flag.Int("budget", defaults.FileSearchBudget, "budget for a file search")
		keywords    = flag.String("keywords", defaults.FileSearchKeywords, "keywords to search a file")
	)

	flag.Parse()
	c := buildClient(hostname, *uiPort, *message, *destination, *file, *request, *budget, strings.Split(*keywords, ","))

	fmt.Println("##### Main Client Parameters ######")
	fmt.Println("Name:", c.Name)
	fmt.Println("Client Address:", c.ClientAddress)
	fmt.Println("Message:", c.Message)
	fmt.Println("Destination:", c.Destination)
	fmt.Println("File:", c.File)
	fmt.Println("Request:", c.Request)
	fmt.Println("Budget:", c.Budget)
	fmt.Println("Keywords:", c.Keywords)
	fmt.Println("###################################")
	fmt.Println("")

	c.Send()
}
