package main

import (
	"flag"
	"fmt"
	"github.com/CedMaire/peerster/src/defaults"
	g "github.com/CedMaire/peerster/src/structs/gossiper"
	"github.com/CedMaire/peerster/src/structs/gossiper/blockchain"
	"github.com/CedMaire/peerster/src/structs/gossiper/database"
	privateDB "github.com/CedMaire/peerster/src/structs/gossiper/database/private"
	f "github.com/CedMaire/peerster/src/structs/gossiper/file"
	"github.com/CedMaire/peerster/src/structs/gossiper/packet"
	"github.com/CedMaire/peerster/src/structs/gossiper/private"
	"github.com/CedMaire/peerster/src/structs/gossiper/route"
	"github.com/CedMaire/peerster/src/structs/gossiper/search"
	"github.com/CedMaire/peerster/src/web"
	"net"
	"os"
	"os/signal"
	"strings"
	"syscall"
)

func buildGossiper(uiPort, guiPort, gossipAddressString, name string,
	simple bool,
	antiEntropy, rtimer int,
	peers []string,
	numberNodes, stubbornTimeout int,
	ackAll, hw3Ex2, hw3Ex3, hw3Ex4 bool) g.Gossiper {
	gossipAddress, err := net.ResolveUDPAddr(defaults.UDP4, gossipAddressString)
	if err != nil {
		panic(err)
	}
	gossipConnection, err := net.ListenUDP(defaults.UDP4, gossipAddress)
	if err != nil {
		panic(err)
	}

	clientAddress, err := net.ResolveUDPAddr(defaults.UDP4, defaults.LocalHost+":"+uiPort)
	if err != nil {
		panic(err)
	}
	clientConnection, err := net.ListenUDP(defaults.UDP4, clientAddress)
	if err != nil {
		panic(err)
	}

	var peersInitial []net.UDPAddr
	var peersLearned []net.UDPAddr

	if len(peers) > 0 && peers[0] != defaults.GossiperPeers {
		for _, peerAddressString := range peers {
			peerAddress, err := net.ResolveUDPAddr(defaults.UDP4, peerAddressString)
			if err != nil {
				panic(err)
			}

			peersInitial = append(peersInitial, *peerAddress)
		}
	}

	db := database.Database{
		Data: make(map[string][]packet.GossipPacket),
	}

	pdb := privateDB.PrivateDB{
		Data: make(map[string][]private.PrivateMessage),
	}

	fdb := f.FileDatabase{
		Data:  make(map[string]f.File),
		Known: make(map[string]f.DiscoveredFile),
	}

	rt := route.RoutingTable{
		Data: make(map[string]route.RouteEntry),
	}

	return g.Gossiper{
		Name:                 name,
		Simple:               simple,
		AntiEntropy:          antiEntropy,
		RouteTimer:           rtimer,
		StubbornTimeout:      stubbornTimeout,
		GuiPort:              guiPort,
		GossipAddress:        *gossipAddress,
		GossipConnection:     *gossipConnection,
		ClientAddress:        *clientAddress,
		ClientConnection:     *clientConnection,
		NumberNodes:          numberNodes,
		Round:                0,
		RoundChangeHistory:   []string{},
		PeersInitial:         peersInitial,
		PeersLearned:         peersLearned,
		Database:             &db,
		PrivateDB:            &pdb,
		FileDB:               &fdb,
		RoutingTable:         &rt,
		AckAll:               ackAll,
		HW3Ex2:               hw3Ex2,
		HW3Ex3:               hw3Ex3,
		HW3Ex4:               hw3Ex4,
		SearchReplies:        []search.SearchReply{},
		BroadcastChannel:     make(chan packet.BroadcastPacket, defaults.ChannelSize),
		AckChannel:           make(chan packet.AckPacket, defaults.ChannelSize),
		TimeoutChannel:       make(chan packet.TimeoutPacket, defaults.ChannelSize),
		IndexChannel:         make(chan string, defaults.ChannelSize),
		DataReplyChannel:     make(chan f.DataReply, defaults.ChannelSize),
		DataRequestChannel:   make(chan f.DataRequest, defaults.ChannelSize),
		SearchRequestChannel: make(chan search.SearchRequest, defaults.ChannelSize),
		SearchReplyChannel:   make(chan search.SearchReply, defaults.ChannelSize),
		BlockPublishChannel:  make(chan blockchain.BlockPublish, defaults.ChannelSize),
		TLCMessageChannel:    make(chan packet.TLCMessage, defaults.ChannelSize),
		TLCAckChannel:        make(chan private.TLCAck, defaults.ChannelSize),
	}
}

func main() {
	hostname, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	hostname = hostname + "-node-groot"

	//noinspection GoBoolExpressions
	var (
		uiPort          = flag.String("UIPort", defaults.GossiperClientUIPort, "port for the ui client")
		guiPort         = flag.String("GUIPort", defaults.GossiperClientGUIPort, "port for the gui client")
		gossipAddress   = flag.String("gossipAddr", defaults.GossiperGossipAddress, "ip:port for the gossiper")
		name            = flag.String("name", hostname, "name of the gossiper")
		simple          = flag.Bool("simple", defaults.GossiperSimple, "run gossiper in simple broadcast mode")
		antiEntropy     = flag.Int("antiEntropy", defaults.GossiperAntiEntropy, "timeout for anti-entropy functionality")
		rtimer          = flag.Int("rtimer", defaults.GossiperRouteTimeout, "timeout for route rumor messages")
		peers           = flag.String("peers", defaults.GossiperPeers, "comma separated list of peers: ip0:port0,ip1:port1,...")
		numberNodes     = flag.Int("N", defaults.NumberNodes, "the number of peerster in the network")
		stubbornTimeout = flag.Int("stubbornTimeout", defaults.StubbornTimeout, "the timeout for re-broadcasting txs")
		ackAll          = flag.Bool("ackAll", defaults.AckAll, "if peerster should ack every tx published")
		hw3Ex2          = flag.Bool("hw3ex2", defaults.HW3Ex2, "if hw3ex2 should be active")
		hw3Ex3          = flag.Bool("hw3ex3", defaults.HW3Ex3, "if hw3ex3 should be active")
		hw3Ex4          = flag.Bool("hw3ex4", defaults.HW3Ex4, "if hw3ex4 should be active")
	)

	flag.Parse()
	gossiper := buildGossiper(*uiPort, *guiPort, *gossipAddress, *name, *simple, *antiEntropy, *rtimer,
		strings.Split(*peers, ","), *numberNodes, *stubbornTimeout, *ackAll, *hw3Ex2, *hw3Ex3, *hw3Ex4)

	gossiper.Print()

	gossiper.Start()
	if !gossiper.Simple {
		go web.StartServer(&gossiper)
	}
	fmt.Println("Listening...")

	signalChannel := make(chan os.Signal, 1)
	signal.Notify(signalChannel, syscall.SIGINT)
	func() {
		<-signalChannel
		fmt.Println("\nTerminating...")
	}()
}
