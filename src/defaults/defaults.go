package defaults

const (
	// LocalHost is the default localhost address.
	LocalHost = "127.0.0.1"

	// UDP4 specifies the default protocol to use.
	UDP4 = "udp4"

	// HopLimit is the maximum number of hops a private message is allowed to traverse before being dropped.
	PrivateHopLimit = 10
	// PrivateID is the default ID for private messages.
	PrivateID = 0

	// BufferSize is the default size for a buffer.
	BufferSize = 1048576
	// ChannelSize = 1024
	// ChannelSize = 1024
	ChannelSize = 1048576
	// ChunkSize is the default size for file chunks (in bytes).
	ChunkSize = 8192
	// FilePath is the default file path.
	FilePath = "NO_FILE"
	// FileSharedBasePath is the default base path from "." to shared files.
	FileSharedBasePath = "/_SharedFiles/"
	// FileDownloadedBasePath is the default base path from "." to downloaded files.
	FileDownloadedBasePath = "/_Downloads/"
	// FileRequest is the default file request.
	FileRequest = "NO_REQUEST"

	// GossiperClientUIPort is the default port for the UI communication.
	GossiperClientUIPort = "8080"
	// GossiperClientGUIPort is the default port for the GUI communication.
	GossiperClientGUIPort = "8181"

	// GossiperGossipPort is the default port for the gossiper.
	GossiperGossipPort = "5000"
	// GossiperGossipAddress is the default address for the gossiper.
	GossiperGossipAddress = LocalHost + ":" + GossiperGossipPort
	// GossiperSimple is the default value for the simple mode of gossiper.
	GossiperSimple = false
	// GossiperAntiEntropy is the default timeout.
	GossiperAntiEntropy = 10
	// GossiperRouteTimeout is the default timeout to send route rumor messages.
	GossiperRouteTimeout = 0
	// GossiperPeers is the default list of peers for gossiper.
	GossiperPeers = "NO_PEERS"
	// GossiperMessageIDStart is the default starting ID for messages.
	GossiperMessageIDStart = 1
	// GossiperMessageAckTimeout is the default timeout for ACK messages.
	GossiperMessageAckTimeout = 10
	// GossiperSentAgo is the default time in seconds to be allowed to resend a message.
	GossiperSentAgo = 1

	// FileManagerTimout is the default timeout for the file manager to trigger new requests.
	FileManagerTimout = 5
	// FileHopeLimit is the default hop-limit for file packets.
	FileHopeLimit = 10

	// FileSearchMinMatches is the matches threshold to stop searching.
	FileSearchMinMatches = 2
	// FileSearchBudget is the default budget for a file search.
	FileSearchBudget     = 2
	FileSearchBudgetMin  = 0
	FileSearchBudgetMax  = 64
	FileSearchMultiplier = 2
	// FileSearchKeywords is the default keywords for a file search.
	FileSearchKeywords = "NO_KEYWORDS"
	// FileSearchInterval is the default interval between file search requests.
	FileSearchInterval = 1
	// FileSearchDropInterval is the default interval to accept the same file requests.
	FileSearchDropInterval = 0.5

	// ClientMessage is the default message to send.
	ClientMessage = "DEFAULT_MESSAGE"
	// Destination is the default destination, empty string means broadcast.
	ClientDestination = "BROADCAST"

	HW3Ex2            = false
	HW3Ex3            = false
	HW3Ex4            = false
	NumberNodes       = -1
	StubbornTimeout   = 5
	AckAll            = false
	TLCMessageSentAgo = 1
)
