package log

import (
	"fmt"
	"net"
)

func Mongering(peer net.UDPAddr) {
	fmt.Println("MONGERING with " + peer.String())
}

func CoinFlip(peer net.UDPAddr) {
	fmt.Println("FLIPPED COIN sending rumor to " + peer.String())
}

func Sync(peer net.UDPAddr) {
	fmt.Println("IN SYNC WITH " + peer.String())
}
