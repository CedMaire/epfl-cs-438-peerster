package database

import (
	"encoding/json"
	"github.com/CedMaire/peerster/src/defaults"
	"github.com/CedMaire/peerster/src/structs/gossiper/packet"
	"github.com/CedMaire/peerster/src/structs/gossiper/status"
	"sync"
)

type Database struct {
	Data map[string][]packet.GossipPacket
}

var dbMutex = &sync.Mutex{}

func (database Database) IsExpected(gossipID uint32, gossipOrigin string) bool {
	if peerMessages, ok := database.Data[gossipOrigin]; ok {
		return uint32(len(peerMessages)+1) == gossipID
	}

	return gossipID == defaults.GossiperMessageIDStart
}

func (database *Database) Add(gossipPacket packet.GossipPacket) {
	dbMutex.Lock()
	defer dbMutex.Unlock()

	var gossipID uint32
	var gossipOrigin string
	if gossipPacket.Rumor != nil {
		gossipID = gossipPacket.Rumor.ID
		gossipOrigin = gossipPacket.Rumor.Origin
	} else if gossipPacket.TLCMessage != nil {
		gossipID = gossipPacket.TLCMessage.ID
		gossipOrigin = gossipPacket.TLCMessage.Origin
	} else {
		panic("gossip packet contains no rumor nor tlc message")
	}

	if gossipID != defaults.GossiperMessageIDStart && database.IsExpected(gossipID, gossipOrigin) {
		database.Data[gossipOrigin] = append(database.Data[gossipOrigin], gossipPacket)
	} else if gossipID == defaults.GossiperMessageIDStart {
		database.Data[gossipOrigin] = []packet.GossipPacket{gossipPacket}
	}
}

func (database Database) ToStatusPacket() status.StatusPacket {
	var peerStatusList []status.PeerStatus
	for node, messages := range database.Data {
		peerStatusList = append(peerStatusList, status.PeerStatus{
			Identifier: node,
			NextID:     uint32(len(messages) + 1),
		})
	}

	return status.StatusPacket{
		Want: peerStatusList,
	}
}

func (database Database) GetSyncState(statusPacket status.StatusPacket) (nextMessage packet.GossipPacket, syncState int) {
	dbMutex.Lock()
	defer dbMutex.Unlock()

	ownStatus := database.ToStatusPacket().ToMap()
	peerStatus := statusPacket.ToMap()

	for node, ownNextID := range ownStatus {
		if peerNextID, ok := peerStatus[node]; ok {
			if ownNextID > peerNextID {
				return database.Data[node][peerNextID-1], 1
			}
		} else {
			return database.Data[node][0], 1
		}
	}

	for node, peerNextID := range peerStatus {
		if ownNextID, ok := ownStatus[node]; ok {
			if peerNextID > ownNextID {
				return packet.GossipPacket{}, -1
			}
		} else {
			return packet.GossipPacket{}, -1
		}
	}

	return packet.GossipPacket{}, 0
}

func (database Database) ToString() string {
	dbMutex.Lock()
	defer dbMutex.Unlock()

	var str = ""
	for origin, gossipPackets := range database.Data {
		str += "\n" + origin + ": "

		for _, gossipPacket := range gossipPackets {
			var out []byte
			var err error
			if gossipPacket.Rumor != nil {
				out, err = json.Marshal(gossipPacket.Rumor)
			} else if gossipPacket.TLCMessage != nil {
				out, err = json.Marshal(gossipPacket.TLCMessage)
			} else {
				panic("gossip packet contains no rumor nor tlc message")
			}

			if err != nil {
				panic(err)
			}

			str += string(out)
		}
	}

	return str
}

func (database Database) GetRounds() (map[string]int, map[string]packet.TLCMessage) {
	rounds := make(map[string]int)
	ms := make(map[string]packet.TLCMessage)
	for origin, messages := range database.Data {
		rounds[origin] = -1

		for _, message := range messages {
			if message.TLCMessage != nil && message.TLCMessage.Confirmed == -1 {
				rounds[origin] += 1
				ms[origin] = *message.TLCMessage
			}
		}
	}

	return rounds, ms
}

func (database Database) HasMajorityConfirmed(numberNodes, myRound int) (bool, []packet.TLCMessage) {
	dbMutex.Lock()
	defer dbMutex.Unlock()

	rounds, messages := database.GetRounds()
	numberAtMyRound := 0

	var newMs []packet.TLCMessage

	for origin, round := range rounds {
		if round == myRound {
			numberAtMyRound += 1
			newMs = append(newMs, messages[origin])
		}
	}

	return numberAtMyRound >= numberNodes/2+1, newMs
}
