package private

import "github.com/CedMaire/peerster/src/structs/gossiper/private"

//noinspection GoNameStartsWithPackageName
type PrivateDB struct {
	Data map[string][]private.PrivateMessage
}

func (privateDatabase *PrivateDB) Add(privateMessage private.PrivateMessage) {
	if _, ok := privateDatabase.Data[privateMessage.Origin]; ok {
		privateDatabase.Data[privateMessage.Origin] = append(privateDatabase.Data[privateMessage.Origin], privateMessage)
	} else {
		privateDatabase.Data[privateMessage.Origin] = []private.PrivateMessage{privateMessage}
	}
}
