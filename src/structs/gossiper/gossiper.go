package gossiper

import (
	"encoding/base64"
	"errors"
	"fmt"
	"github.com/CedMaire/peerster/src/defaults"
	"github.com/CedMaire/peerster/src/log"
	"github.com/CedMaire/peerster/src/structs/addr"
	cm "github.com/CedMaire/peerster/src/structs/client/message"
	"github.com/CedMaire/peerster/src/structs/gossiper/blockchain"
	"github.com/CedMaire/peerster/src/structs/gossiper/database"
	privateDB "github.com/CedMaire/peerster/src/structs/gossiper/database/private"
	f "github.com/CedMaire/peerster/src/structs/gossiper/file"
	"github.com/CedMaire/peerster/src/structs/gossiper/packet"
	"github.com/CedMaire/peerster/src/structs/gossiper/private"
	"github.com/CedMaire/peerster/src/structs/gossiper/route"
	"github.com/CedMaire/peerster/src/structs/gossiper/rumor"
	"github.com/CedMaire/peerster/src/structs/gossiper/search"
	"github.com/CedMaire/peerster/src/structs/gossiper/status"
	sm "github.com/CedMaire/peerster/src/structs/simple/message"
	"github.com/dedis/protobuf"
	"math"
	"math/rand"
	"net"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Structure
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

type Gossiper struct {
	Name                 string
	Simple               bool
	AntiEntropy          int
	RouteTimer           int
	StubbornTimeout      int
	GuiPort              string
	GossipAddress        net.UDPAddr
	GossipConnection     net.UDPConn
	ClientAddress        net.UDPAddr
	ClientConnection     net.UDPConn
	NumberNodes          int
	Round                int
	RoundChangeHistory   []string
	PeersInitial         []net.UDPAddr
	PeersLearned         []net.UDPAddr
	Database             *database.Database
	PrivateDB            *privateDB.PrivateDB
	FileDB               *f.FileDatabase
	RoutingTable         *route.RoutingTable
	AckAll               bool
	HW3Ex2               bool
	HW3Ex3               bool
	HW3Ex4               bool
	SearchReplies        []search.SearchReply
	BroadcastChannel     chan packet.BroadcastPacket
	AckChannel           chan packet.AckPacket
	TimeoutChannel       chan packet.TimeoutPacket
	IndexChannel         chan string
	DataReplyChannel     chan f.DataReply
	DataRequestChannel   chan f.DataRequest
	SearchRequestChannel chan search.SearchRequest
	SearchReplyChannel   chan search.SearchReply
	BlockPublishChannel  chan blockchain.BlockPublish
	TLCMessageChannel    chan packet.TLCMessage
	TLCAckChannel        chan private.TLCAck
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// General Functions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

func (gossiper *Gossiper) Start() {
	if gossiper.Simple {
		go gossiper.simpleSender()
	} else {
		go gossiper.rumorSender()
		go gossiper.fileManager()
		go gossiper.blockHandler()
	}

	go gossiper.Listener(false)
	go gossiper.Listener(true)
}

func (gossiper *Gossiper) Listener(isClient bool) {
	connection := net.UDPConn{}
	if isClient {
		connection = gossiper.ClientConnection
	} else {
		connection = gossiper.GossipConnection
	}

	packetChan := make(chan addr.Packet, defaults.ChannelSize)
	go addr.ReadSocket(connection, packetChan)
	if isClient && !gossiper.Simple && gossiper.RouteTimer > 0 {
		go addr.Route(gossiper.RouteTimer, packetChan)
	}

	for {
		select {
		case packetSocket := <-packetChan:
			if gossiper.Simple {
				gossiper.simpleHandler(packetSocket.Bytes, packetSocket.Relay, isClient)
			} else {
				gossiper.rumorHandler(packetSocket.Bytes, packetSocket.Relay, isClient)
			}
		}
	}
}

func (gossiper *Gossiper) LogPeers() {
	var toPrint = "PEERS "
	for _, address := range append(gossiper.PeersInitial, gossiper.PeersLearned...) {
		toPrint += address.String() + ","
	}
	toPrint = strings.TrimSuffix(toPrint, ",")

	fmt.Println(toPrint)
}

func (gossiper *Gossiper) Print() {
	fmt.Println("\n##### Main Gossiper Parameters ######")
	fmt.Println("Name:", gossiper.Name)
	fmt.Println("Simple:", gossiper.Simple)
	fmt.Println("Anti Entropy:", gossiper.AntiEntropy)
	fmt.Println("Route Timer:", gossiper.RouteTimer)
	fmt.Println("GUI Port:", gossiper.GuiPort)
	fmt.Println("Gossip Address:", gossiper.GossipAddress)
	fmt.Println("Client Address:", gossiper.ClientAddress)
	fmt.Println("Peers Initial:", gossiper.PeersInitial)
	fmt.Println("NumberNodes:", gossiper.NumberNodes)
	fmt.Println("StubbornTimeout:", gossiper.StubbornTimeout)
	fmt.Println("AckAll:", gossiper.AckAll)
	fmt.Println("HW3Ex2:", gossiper.HW3Ex2)
	fmt.Println("HW3Ex3:", gossiper.HW3Ex3)
	fmt.Println("HW3Ex4:", gossiper.HW3Ex4)
	//noinspection GoPrintFunctions
	fmt.Println("#####################################\n")
}

func (gossiper *Gossiper) PrintState() {
	fmt.Println("")
	fmt.Println("##### Gossiper State ######")
	fmt.Println("Peers:", append(gossiper.PeersInitial, gossiper.PeersLearned...))
	fmt.Println("Database:", gossiper.Database.ToString())
	fmt.Println("Private Database:", gossiper.PrivateDB)
	fmt.Println("File Database:", gossiper.FileDB.GetKeys())
	fmt.Println("Routing Table:", gossiper.RoutingTable)
	fmt.Println("############################")
	fmt.Println("")
}

func stringArrayContains(stringArray []string, s string) bool {
	for _, tmpString := range stringArray {
		if tmpString == s {
			return true
		}
	}

	return false
}

var nextIDMutex = &sync.Mutex{}

func (gossiper *Gossiper) nextID() uint32 {
	nextIDMutex.Lock()
	defer nextIDMutex.Unlock()

	if messages, ok := gossiper.Database.Data[gossiper.Name]; ok {
		return uint32(len(messages) + 1)
	}

	return defaults.GossiperMessageIDStart
}

func (gossiper *Gossiper) getRandomPeer(excluded []net.UDPAddr) (net.UDPAddr, error) {
	var allPeers []net.UDPAddr
	for _, peer := range append(gossiper.PeersInitial, gossiper.PeersLearned...) {
		if !addr.Contains(excluded, peer) {
			allPeers = append(allPeers, peer)
		}
	}

	if len(allPeers) > 0 {
		return allPeers[rand.Intn(len(allPeers))], nil
	} else {
		return net.UDPAddr{}, errors.New("no peer available")
	}
}

func (gossiper *Gossiper) randomBroadcast(gossipPacket packet.GossipPacket, excluded []net.UDPAddr) (packet.BroadcastPacket, error) {
	peer, err := gossiper.getRandomPeer(excluded)
	if err != nil {
		return packet.BroadcastPacket{}, err
	}

	return packet.BroadcastPacket{
		Packet:       gossipPacket,
		Destinations: []net.UDPAddr{peer},
	}, nil
}

func (gossiper *Gossiper) send(gossipPacket packet.GossipPacket, address net.UDPAddr) {
	if gossipPacket.Simple != nil ||
		gossipPacket.Rumor != nil ||
		gossipPacket.Status != nil ||
		gossipPacket.Private != nil ||
		gossipPacket.DataReply != nil ||
		gossipPacket.DataRequest != nil ||
		gossipPacket.SearchRequest != nil ||
		gossipPacket.SearchReply != nil ||
		gossipPacket.TLCMessage != nil ||
		gossipPacket.Ack != nil {
		packetBytes, err := protobuf.Encode(&gossipPacket)
		if err != nil {
			fmt.Println("ERROR: " + err.Error())
			return
		}

		if _, err = gossiper.GossipConnection.WriteToUDP(packetBytes, &address); err != nil {
			fmt.Println("ERROR: " + err.Error())
			return
		}
	}
}

func (gossiper *Gossiper) sendStatusPacket(address net.UDPAddr) {
	statusPacket := gossiper.Database.ToStatusPacket()

	gossiper.BroadcastChannel <- packet.BroadcastPacket{
		Packet: packet.GossipPacket{
			Status: &statusPacket,
		},
		Destinations: []net.UDPAddr{address},
	}
}

func (gossiper *Gossiper) forward(gossipPacket packet.GossipPacket) (packet.BroadcastPacket, error) {
	destination := ""
	if gossipPacket.Private != nil {
		destination = gossipPacket.Private.Destination
	} else if gossipPacket.DataReply != nil {
		destination = gossipPacket.DataReply.Destination
	} else if gossipPacket.DataRequest != nil {
		destination = gossipPacket.DataRequest.Destination
	} else if gossipPacket.SearchReply != nil {
		destination = gossipPacket.SearchReply.Destination
	} else if gossipPacket.Ack != nil {
		destination = gossipPacket.Ack.Destination
	} else {
		return packet.BroadcastPacket{}, errors.New("gossip packet is not routable")
	}

	if routeEntry, ok := gossiper.RoutingTable.Data[destination]; ok {
		return packet.BroadcastPacket{
			Packet:       gossipPacket,
			Destinations: []net.UDPAddr{routeEntry.Addr},
		}, nil
	}

	return packet.BroadcastPacket{}, errors.New("unknown route for destination " + destination)
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Simple Mode Functions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

func (gossiper *Gossiper) simpleSender() {
	var messages []sm.SimpleMessage

	for {
		select {
		case broadcastPacket := <-gossiper.BroadcastChannel:
			for _, peer := range broadcastPacket.Destinations {
				if !broadcastPacket.Packet.Simple.Contained(messages) {
					messages = append(messages, *broadcastPacket.Packet.Simple)
				}

				for _, message := range messages {
					gossiper.send(packet.GossipPacket{
						Simple: &message,
					}, peer)
				}
			}
		}
	}
}

func (gossiper *Gossiper) simpleHandlerClient(bytes []byte) (packet.GossipPacket, error) {
	m := cm.Message{}
	if err := protobuf.Decode(bytes, &m); err != nil {
		return packet.GossipPacket{}, err
	}

	m.Log()

	return packet.GossipPacket{
		Simple: &sm.SimpleMessage{
			OriginalName:     gossiper.Name,
			RelayPeerAddress: gossiper.GossipConnection.LocalAddr().String(),
			Contents:         m.Text,
		},
	}, nil
}

func (gossiper *Gossiper) simpleHandlerPeer(bytes []byte, relay net.UDPAddr) (packet.GossipPacket, error) {
	gossipPacket := packet.GossipPacket{}
	if err := protobuf.Decode(bytes, &gossipPacket); err != nil {
		return packet.GossipPacket{}, err
	}
	if gossipPacket.Simple == nil {
		return packet.GossipPacket{}, errors.New("no simple packet inside GossipPacket")
	}

	gossipPacket.Simple.Log()

	if !addr.Contains(append(gossiper.PeersInitial, gossiper.PeersLearned...), relay) {
		gossiper.PeersLearned = append(gossiper.PeersLearned, relay)
	}

	gossipPacket.Simple.RelayPeerAddress = gossiper.GossipConnection.LocalAddr().String()

	return gossipPacket, nil
}

func (gossiper *Gossiper) simpleHandler(bytes []byte, relayAddr net.Addr, isClient bool) {
	relay, err := net.ResolveUDPAddr(defaults.UDP4, relayAddr.String())
	if err != nil {
		return
	}

	var destinations []net.UDPAddr
	for _, destination := range append(gossiper.PeersInitial, gossiper.PeersLearned...) {
		if !addr.Equals(*relay, destination) {
			destinations = append(destinations, destination)
		}
	}

	gossipPacket := packet.GossipPacket{}
	if isClient {
		gossipPacket, err = gossiper.simpleHandlerClient(bytes)
	} else {
		gossipPacket, err = gossiper.simpleHandlerPeer(bytes, *relay)
	}
	if err != nil {
		fmt.Println("ERROR: " + err.Error())
		return
	}

	gossiper.LogPeers()
	gossiper.BroadcastChannel <- packet.BroadcastPacket{
		Packet:       gossipPacket,
		Destinations: destinations,
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Rumor Mode Functions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

func (gossiper *Gossiper) timeout(peer net.UDPAddr, rumorMessage rumor.RumorMessage) {
	t := time.NewTicker(time.Duration(defaults.GossiperMessageAckTimeout) * time.Second)
	<-t.C

	gossiper.TimeoutChannel <- packet.TimeoutPacket{
		Peer:   peer,
		Packet: rumorMessage,
	}
}

func (gossiper *Gossiper) blockHandler() {
	blockPublishTimeout := time.NewTicker(time.Duration(defaults.StubbornTimeout) * time.Second)

	blocks := make(map[string]blockchain.BlockPublish)
	tlcsUnconfirmed := make(map[string]packet.TLCMessage)
	tlcsConfirmed := make(map[string]packet.TLCMessage)
	acks := make(map[string][]string)

	storedBlockPublish := make(map[int]blockchain.BlockPublish)

	myRound := 0
	receivedForMyRound := false
	sentFromStorage := false

	for {
		select {
		case _ = <-blockPublishTimeout.C:
			for id := range blocks {
				if len(acks[id]) < gossiper.NumberNodes/2+1 {
					tlcMessage := tlcsUnconfirmed[id]

					broadcastPacket, err := gossiper.randomBroadcast(packet.GossipPacket{
						TLCMessage: &tlcMessage,
					}, []net.UDPAddr{})
					if err != nil {
						fmt.Println("ERROR: " + err.Error())
						continue
					}

					gossiper.BroadcastChannel <- broadcastPacket

					tlcMessage.LogUnconfirmed()
				} else if _, ok := tlcsUnconfirmed[id]; ok {
					tlcMessage := packet.TLCMessage{
						Origin:      tlcsUnconfirmed[id].Origin,
						ID:          gossiper.nextID(),
						Confirmed:   int(tlcsUnconfirmed[id].ID),
						TxBlock:     tlcsUnconfirmed[id].TxBlock,
						VectorClock: tlcsUnconfirmed[id].VectorClock,
						Fitness:     tlcsUnconfirmed[id].Fitness,
					}
					tlcsConfirmed[id] = tlcMessage
					delete(tlcsUnconfirmed, id)

					broadcastPacket, err := gossiper.randomBroadcast(packet.GossipPacket{
						TLCMessage: &tlcMessage,
					}, []net.UDPAddr{})
					if err != nil {
						fmt.Println("ERROR: " + err.Error())
						continue
					}

					gossiper.BroadcastChannel <- broadcastPacket

					tlcsConfirmed[id].LogReBroadcast(acks[id])

					gossiper.Database.Add(packet.GossipPacket{
						TLCMessage: &tlcMessage,
					})
				}
			}

			hasMaj, ms := gossiper.Database.HasMajorityConfirmed(gossiper.NumberNodes, myRound)
			if hasMaj && receivedForMyRound {
				tlcsUnconfirmed = make(map[string]packet.TLCMessage)

				myRound += 1
				gossiper.Round = myRound
				receivedForMyRound = false
				roundChangeLog := packet.LogRoundChange(myRound, ms)
				gossiper.RoundChangeHistory = append(gossiper.RoundChangeHistory, roundChangeLog)

				if _, ok := storedBlockPublish[myRound]; ok {
					sentFromStorage = true
					gossiper.BlockPublishChannel <- storedBlockPublish[myRound]
				}
			}
		case blockPublish := <-gossiper.BlockPublishChannel:
			if !receivedForMyRound && len(storedBlockPublish) == 0 || sentFromStorage {
				sentFromStorage = false
				if _, ok := blocks[base64.StdEncoding.EncodeToString(blockPublish.Transaction.MetafileHash)]; !ok {
					blocks[base64.StdEncoding.EncodeToString(blockPublish.Transaction.MetafileHash)] = blockPublish

					tlcMessage := packet.TLCMessage{
						Origin:      gossiper.Name,
						ID:          gossiper.nextID(),
						Confirmed:   -1,
						TxBlock:     blockPublish,
						VectorClock: nil,
						Fitness:     0,
					}

					tlcsUnconfirmed[base64.StdEncoding.EncodeToString(blockPublish.Transaction.MetafileHash)] = tlcMessage
					acks[base64.StdEncoding.EncodeToString(blockPublish.Transaction.MetafileHash)] =
						append(acks[base64.StdEncoding.EncodeToString(blockPublish.Transaction.MetafileHash)], gossiper.Name)

					broadcastPacket, err := gossiper.randomBroadcast(packet.GossipPacket{
						TLCMessage: &tlcMessage,
					}, []net.UDPAddr{})

					if err != nil {
						fmt.Println("ERROR: " + err.Error())
						continue
					}

					gossiper.BroadcastChannel <- broadcastPacket
					tlcMessage.LogUnconfirmed()

					gossiper.Database.Add(packet.GossipPacket{
						TLCMessage: &tlcMessage,
					})

					receivedForMyRound = true
				}
			} else {
				storedBlockPublish[int(math.Max(float64(len(storedBlockPublish)+1), float64(myRound+1)))] = blockPublish
			}
		case tlcMessage := <-gossiper.TLCMessageChannel:
			if _, ok := tlcsUnconfirmed[base64.StdEncoding.EncodeToString(tlcMessage.TxBlock.Transaction.MetafileHash)]; !ok {
				tlcsUnconfirmed[base64.StdEncoding.EncodeToString(tlcMessage.TxBlock.Transaction.MetafileHash)] = tlcMessage
			}

			gossiper.Database.Add(packet.GossipPacket{
				TLCMessage: &tlcMessage,
			})

			syncState := 0
			if tlcMessage.VectorClock != nil {
				_, syncState = gossiper.Database.GetSyncState(*tlcMessage.VectorClock)
			}

			rounds, _ := gossiper.Database.GetRounds()
			if gossiper.AckAll || (rounds[tlcMessage.Origin] >= myRound && syncState == 0) {
				tlcAck := private.TLCAck{
					Origin:      gossiper.Name,
					ID:          tlcMessage.ID,
					Text:        "",
					Destination: tlcMessage.Origin,
					HopLimit:    defaults.PrivateHopLimit - 1,
				}

				tlcAck.Log()

				broadcastPacket, err := gossiper.forward(packet.GossipPacket{
					Ack: &tlcAck,
				})
				if err != nil {
					fmt.Println("ERROR: " + err.Error())
					continue
				}

				gossiper.BroadcastChannel <- broadcastPacket
			}
		case tlcAck := <-gossiper.TLCAckChannel:
			for id := range blocks {
				if tlcsUnconfirmed[id].ID == tlcAck.ID {
					if !stringArrayContains(acks[id], tlcAck.Origin) {
						acks[id] = append(acks[id], tlcAck.Origin)

						if len(acks[id]) >= gossiper.NumberNodes/2+1 {
							tlcMessage := packet.TLCMessage{
								Origin:      tlcsUnconfirmed[id].Origin,
								ID:          gossiper.nextID(),
								Confirmed:   int(tlcsUnconfirmed[id].ID),
								TxBlock:     tlcsUnconfirmed[id].TxBlock,
								VectorClock: tlcsUnconfirmed[id].VectorClock,
								Fitness:     tlcsUnconfirmed[id].Fitness,
							}
							tlcsConfirmed[id] = tlcMessage
							delete(tlcsUnconfirmed, id)

							broadcastPacket, err := gossiper.randomBroadcast(packet.GossipPacket{
								TLCMessage: &tlcMessage,
							}, []net.UDPAddr{})
							if err != nil {
								fmt.Println("ERROR: " + err.Error())
								continue
							}

							gossiper.BroadcastChannel <- broadcastPacket

							tlcsConfirmed[id].LogReBroadcast(acks[id])
							gossiper.Database.Add(packet.GossipPacket{
								TLCMessage: &tlcMessage,
							})
						}
					}

					break
				}
			}
		}
	}
}

func (gossiper *Gossiper) fileManager() {
	downloadTicker := time.NewTicker(time.Duration(defaults.FileManagerTimout) * time.Second)
	discoveryTicker := time.NewTicker(time.Duration(defaults.FileSearchInterval) * time.Second)

	ongoingSearchRequest := make(map[int]search.SearchRequest)
	receivedSearchRequests := make(map[string]time.Time)

	for {
		select {
		case _ = <-downloadTicker.C:
			gossiper.FileDB.WriteToDisc()

			for _, dataRequest := range gossiper.FileDB.GetNextDataRequests(gossiper.Name) {
				broadcastPacket, err := gossiper.forward(packet.GossipPacket{
					DataRequest: &dataRequest,
				})
				if err != nil {
					fmt.Println("ERROR: " + err.Error())
					continue
				}

				gossiper.BroadcastChannel <- broadcastPacket
			}
		case _ = <-discoveryTicker.C:
			if len(ongoingSearchRequest) == 1 {
				searchRequest := ongoingSearchRequest[0]
				if gossiper.FileDB.SearchMatches(searchRequest.Keywords) < defaults.FileSearchMinMatches &&
					searchRequest.Budget < defaults.FileSearchBudgetMax {
					ongoingSearchRequest[0] = search.SearchRequest{
						Origin: searchRequest.Origin,
						Budget: uint64(math.Min(float64(defaults.FileSearchMultiplier*searchRequest.Budget),
							defaults.FileSearchBudgetMax)),
						Keywords: searchRequest.Keywords,
					}

					searchRequest = ongoingSearchRequest[0]
					for _, budAddr := range search.GetBudgetedDistribution(append(gossiper.PeersInitial, gossiper.PeersLearned...),
						searchRequest.Budget) {
						gossiper.BroadcastChannel <- packet.BroadcastPacket{
							Packet: packet.GossipPacket{
								SearchRequest: &search.SearchRequest{
									Origin:   searchRequest.Origin,
									Budget:   budAddr.Budget,
									Keywords: searchRequest.Keywords,
								},
							},
							Destinations: []net.UDPAddr{budAddr.Address},
						}
					}
				} else {
					delete(ongoingSearchRequest, 0)
					search.LogSearchFinished(gossiper.FileDB.SearchMatches(searchRequest.Keywords))
				}
			} else if len(ongoingSearchRequest) > 1 {
				fmt.Println("ERROR: ongoingSearchRequest has more than a single element")
			}
		case fileName := <-gossiper.IndexChannel:
			blockPublish, err := gossiper.FileDB.Index(fileName, gossiper.Name)
			if err != nil {
				fmt.Println("ERROR: " + err.Error())
				continue
			}

			gossiper.BlockPublishChannel <- blockPublish
		case dataReply := <-gossiper.DataReplyChannel:
			done, dataRequest := gossiper.FileDB.UpdateFile(dataReply)

			if !done {
				broadcastPacket, err := gossiper.forward(packet.GossipPacket{
					DataRequest: &dataRequest,
				})
				if err != nil {
					fmt.Println("ERROR: " + err.Error())
				} else {
					gossiper.BroadcastChannel <- broadcastPacket
				}
			}
		case dataRequest := <-gossiper.DataRequestChannel:
			dataReply := gossiper.FileDB.Reply(dataRequest)

			broadcastPacket, err := gossiper.forward(packet.GossipPacket{
				DataReply: &dataReply,
			})
			if err != nil {
				fmt.Println("ERROR: " + err.Error())
			} else {
				gossiper.BroadcastChannel <- broadcastPacket
			}
		case searchRequest := <-gossiper.SearchRequestChannel:
			if searchRequest.Origin == gossiper.Name {
				if len(ongoingSearchRequest) == 0 {
					gossiper.SearchReplies = []search.SearchReply{}
					ongoingSearchRequest[0] = searchRequest
				}
			} else {
				keywords := make([]string, len(searchRequest.Keywords))
				copy(keywords, searchRequest.Keywords)
				sort.Strings(keywords)

				referencer := searchRequest.Origin + strings.Join(keywords, "")
				if lastReceived, ok := receivedSearchRequests[referencer];
					!ok || (ok && time.Now().Sub(lastReceived).Seconds() > defaults.FileSearchDropInterval) {
					match, searchReply := gossiper.FileDB.GetSearchReply(searchRequest, gossiper.Name)
					if match {
						broadcastPacket, err := gossiper.forward(packet.GossipPacket{
							SearchReply: &searchReply,
						})

						if err != nil {
							fmt.Println("ERROR: " + err.Error())
						} else {
							gossiper.BroadcastChannel <- broadcastPacket
						}
					}

					if searchRequest.Budget >= 2 {
						for _, budAddr := range search.GetBudgetedDistribution(append(gossiper.PeersInitial, gossiper.PeersLearned...),
							searchRequest.Budget-1) {
							gossiper.BroadcastChannel <- packet.BroadcastPacket{
								Packet: packet.GossipPacket{
									SearchRequest: &search.SearchRequest{
										Origin:   searchRequest.Origin,
										Budget:   budAddr.Budget,
										Keywords: searchRequest.Keywords,
									},
								},
								Destinations: []net.UDPAddr{budAddr.Address},
							}
						}
					}
				}
			}
		case searchReply := <-gossiper.SearchReplyChannel:
			gossiper.SearchReplies = append(gossiper.SearchReplies, searchReply)
			gossiper.FileDB.UpdateDiscoveredFile(searchReply)
		}
	}
}

func (gossiper *Gossiper) rumorSender() {
	antiEntropy := time.NewTicker(time.Duration(gossiper.AntiEntropy) * time.Second)
	timeoutMap := make(map[string][]rumor.RumorMessage)
	sentAgo := make(map[string]time.Time)

	if peer, err := gossiper.getRandomPeer([]net.UDPAddr{}); err == nil {
		gossiper.sendStatusPacket(peer)
	}

	for {
		select {
		case _ = <-antiEntropy.C:
			if peer, err := gossiper.getRandomPeer([]net.UDPAddr{}); err == nil {
				gossiper.sendStatusPacket(peer)
			}

			gossiper.PrintState()
		case ackPacket := <-gossiper.AckChannel:
			if _, ok := timeoutMap[ackPacket.From.String()]; ok {
				for _, s := range ackPacket.Packet.Want {
					timeoutMap[ackPacket.From.String()] = rumor.RumorMessage{
						Origin: s.Identifier,
						ID:     s.NextID - 1,
					}.RemoveFrom(timeoutMap[ackPacket.From.String()])

					if len(timeoutMap[ackPacket.From.String()]) == 0 {
						delete(timeoutMap, ackPacket.From.String())
					}
				}
			}
		case timeoutPacket := <-gossiper.TimeoutChannel:
			if _, ok := timeoutMap[timeoutPacket.Peer.String()]; ok {
				timeoutMap[timeoutPacket.Peer.String()] = timeoutPacket.Packet.RemoveFrom(timeoutMap[timeoutPacket.Peer.String()])

				if len(timeoutMap[timeoutPacket.Peer.String()]) == 0 {
					delete(timeoutMap, timeoutPacket.Peer.String())
				}

				broadcastPacket, err := gossiper.randomBroadcast(packet.GossipPacket{
					Rumor: &timeoutPacket.Packet,
				}, []net.UDPAddr{timeoutPacket.Peer})
				if err != nil {
					fmt.Println("ERROR: " + err.Error())
					continue
				}

				gossiper.BroadcastChannel <- broadcastPacket
			}
		case broadcastPacket := <-gossiper.BroadcastChannel:
			for _, peer := range broadcastPacket.Destinations {
				if broadcastPacket.Packet.Rumor != nil && !broadcastPacket.Packet.Rumor.Contained(timeoutMap[peer.String()]) {
					if lastSent, ok := sentAgo[broadcastPacket.Packet.Rumor.Origin+strconv.Itoa(
						int(broadcastPacket.Packet.Rumor.ID))+peer.String()];
						!ok || (time.Now().Sub(lastSent).Seconds() > defaults.GossiperSentAgo) {
						sentAgo[broadcastPacket.Packet.Rumor.Origin+strconv.Itoa(
							int(broadcastPacket.Packet.Rumor.ID))+peer.String()] = time.Now()

						gossiper.send(broadcastPacket.Packet, peer)
						log.Mongering(peer)

						if _, ok := timeoutMap[peer.String()]; ok {
							timeoutMap[peer.String()] = broadcastPacket.Packet.Rumor.AddTo(timeoutMap[peer.String()])
						} else {
							timeoutMap[peer.String()] = []rumor.RumorMessage{*broadcastPacket.Packet.Rumor}
						}

						go gossiper.timeout(peer, *broadcastPacket.Packet.Rumor)
					}
				} else if broadcastPacket.Packet.TLCMessage != nil {
					if lastSent, ok := sentAgo[base64.StdEncoding.EncodeToString(
						broadcastPacket.Packet.TLCMessage.TxBlock.Transaction.MetafileHash)];
						!ok || (ok && time.Now().Sub(lastSent).Seconds() > defaults.TLCMessageSentAgo) {
						gossiper.send(broadcastPacket.Packet, peer)
						sentAgo[base64.StdEncoding.EncodeToString(
							broadcastPacket.Packet.TLCMessage.TxBlock.Transaction.MetafileHash)] = time.Now()
					}
				} else if broadcastPacket.Packet.Status != nil ||
					broadcastPacket.Packet.Private != nil ||
					broadcastPacket.Packet.DataReply != nil ||
					broadcastPacket.Packet.DataRequest != nil ||
					broadcastPacket.Packet.SearchRequest != nil ||
					broadcastPacket.Packet.SearchReply != nil ||
					broadcastPacket.Packet.Ack != nil {

					gossiper.send(broadcastPacket.Packet, peer)
				}
			}
		}
	}
}

func (gossiper *Gossiper) rumorHandlerClientPrivate(message cm.Message) (packet.BroadcastPacket, error) {
	gossipPacket := packet.GossipPacket{
		Private: &private.PrivateMessage{
			Origin:      gossiper.Name,
			ID:          defaults.PrivateID,
			Text:        message.Text,
			Destination: *message.Destination,
			HopLimit:    defaults.PrivateHopLimit - 1,
		},
	}

	return gossiper.forward(gossipPacket)
}

func (gossiper *Gossiper) rumorHandlerClientFileIndex(message cm.Message) (packet.BroadcastPacket, error) {
	gossiper.IndexChannel <- *message.File

	return packet.BroadcastPacket{}, nil
}

func (gossiper *Gossiper) rumorHandlerClientRumor(message cm.Message) (packet.BroadcastPacket, error) {
	gossipPacket := packet.GossipPacket{
		Rumor: &rumor.RumorMessage{
			Origin: gossiper.Name,
			ID:     gossiper.nextID(),
			Text:   message.Text,
		},
	}

	gossiper.Database.Add(gossipPacket)

	return gossiper.randomBroadcast(gossipPacket, []net.UDPAddr{})
}

func (gossiper *Gossiper) rumorHandlerClientFileRequest(message cm.Message) (packet.BroadcastPacket, error) {
	dataRequest, err := gossiper.FileDB.AddNewFileToDownload(message, gossiper.Name)
	if err != nil {
		return packet.BroadcastPacket{}, err
	}

	return gossiper.forward(packet.GossipPacket{
		DataRequest: &dataRequest,
	})
}

func (gossiper *Gossiper) rumorHandlerClientSearchRequest(message cm.Message) (packet.BroadcastPacket, error) {
	gossiper.SearchRequestChannel <- search.SearchRequest{
		Origin:   gossiper.Name,
		Budget:   *message.Budget,
		Keywords: *message.Keywords,
	}

	return packet.BroadcastPacket{}, nil
}

func (gossiper *Gossiper) RumorHandlerClient(bytes []byte) (packet.BroadcastPacket, error) {
	m := cm.Message{}
	if err := protobuf.Decode(bytes, &m); err != nil {
		return packet.BroadcastPacket{}, err
	}

	if m.Text != "" {
		m.Log()
	}

	if m.IsFileRequest() {
		return gossiper.rumorHandlerClientFileRequest(m)
	} else if m.IsFileIndex() {
		return gossiper.rumorHandlerClientFileIndex(m)
	} else if m.IsPrivate() {
		return gossiper.rumorHandlerClientPrivate(m)
	} else if m.IsFileSearch() {
		return gossiper.rumorHandlerClientSearchRequest(m)
	} else {
		return gossiper.rumorHandlerClientRumor(m)
	}
}

var originalMessages []net.UDPAddr

func (gossiper *Gossiper) rumorHandlerPeerAck(statusPacket status.StatusPacket, relay net.UDPAddr) (packet.BroadcastPacket, error) {
	statusPacket.LogPacket(relay)

	gossiper.AckChannel <- packet.AckPacket{
		From:   relay,
		Packet: statusPacket,
	}

	gossipPacket, syncState := gossiper.Database.GetSyncState(statusPacket)
	if syncState > 0 {
		return packet.BroadcastPacket{
			Packet:       gossipPacket,
			Destinations: []net.UDPAddr{relay},
		}, nil
	} else if syncState < 0 {
		gossiper.sendStatusPacket(relay)

		return packet.BroadcastPacket{}, nil
	} else {
		log.Sync(relay)

		if addr.Contains(originalMessages, relay) {
			originalMessages = addr.Remove(originalMessages, relay)

			rand.Seed(time.Now().UnixNano())
			if coin := rand.Intn(2); coin == 1 {
				log.CoinFlip(relay)

				if peer, err := gossiper.getRandomPeer([]net.UDPAddr{relay}); err == nil {
					gossiper.sendStatusPacket(peer)
				} else {
					return packet.BroadcastPacket{}, err
				}
			}
		}

		return packet.BroadcastPacket{}, nil
	}
}

func (gossiper *Gossiper) rumorHandlerPeerRumor(rumorMessage rumor.RumorMessage, relay net.UDPAddr) (packet.BroadcastPacket, error) {
	rumorMessage.Log(relay)

	gossiper.Database.Add(packet.GossipPacket{
		Rumor: &rumorMessage,
	})
	if rumorMessage.Origin != gossiper.Name {
		gossiper.RoutingTable.Update(rumorMessage, relay)
	}
	originalMessages = addr.Add(originalMessages, relay)

	gossiper.sendStatusPacket(relay)
	return gossiper.randomBroadcast(packet.GossipPacket{
		Rumor: &rumorMessage,
	}, []net.UDPAddr{relay})
}

func (gossiper *Gossiper) rumorHandlerPeerPrivate(privateMessage private.PrivateMessage) (packet.BroadcastPacket, error) {
	if privateMessage.Destination == gossiper.Name {
		privateMessage.Log()
		gossiper.PrivateDB.Add(privateMessage)

		return packet.BroadcastPacket{}, nil
	}

	if privateMessage.HopLimit <= 1 {
		return packet.BroadcastPacket{}, errors.New("hop-limit reached")
	}

	gossipPacket := packet.GossipPacket{
		Private: &private.PrivateMessage{
			Origin:      privateMessage.Origin,
			ID:          defaults.PrivateID,
			Text:        privateMessage.Text,
			Destination: privateMessage.Destination,
			HopLimit:    privateMessage.HopLimit - 1,
		},
	}

	return gossiper.forward(gossipPacket)
}

func (gossiper *Gossiper) rumorHandlerPeerDataReply(dataReply f.DataReply) (packet.BroadcastPacket, error) {
	if dataReply.Destination == gossiper.Name {
		gossiper.DataReplyChannel <- dataReply

		return packet.BroadcastPacket{}, nil
	}

	if dataReply.HopLimit <= 1 {
		return packet.BroadcastPacket{}, errors.New("hop-limit reached")
	}

	gossipPacket := packet.GossipPacket{
		DataReply: &f.DataReply{
			Origin:      dataReply.Origin,
			Destination: dataReply.Destination,
			HopLimit:    dataReply.HopLimit - 1,
			HashValue:   dataReply.HashValue,
			Data:        dataReply.Data,
		},
	}

	return gossiper.forward(gossipPacket)
}

func (gossiper *Gossiper) rumorHandlerPeerDataRequest(dataRequest f.DataRequest) (packet.BroadcastPacket, error) {
	if dataRequest.Destination == gossiper.Name {
		gossiper.DataRequestChannel <- dataRequest

		return packet.BroadcastPacket{}, nil
	}

	if dataRequest.HopLimit <= 1 {
		return packet.BroadcastPacket{}, errors.New("hop-limit reached")
	}

	gossipPacket := packet.GossipPacket{
		DataRequest: &f.DataRequest{
			Origin:      dataRequest.Origin,
			Destination: dataRequest.Destination,
			HopLimit:    dataRequest.HopLimit - 1,
			HashValue:   dataRequest.HashValue,
		},
	}

	return gossiper.forward(gossipPacket)
}

func (gossiper *Gossiper) rumorHandlerPeerSearchRequest(searchRequest search.SearchRequest) (packet.BroadcastPacket, error) {
	gossiper.SearchRequestChannel <- searchRequest

	return packet.BroadcastPacket{}, nil
}

func (gossiper *Gossiper) rumorHandlerPeerSearchReply(searchReply search.SearchReply) (packet.BroadcastPacket, error) {
	if searchReply.Destination == gossiper.Name {
		gossiper.SearchReplyChannel <- searchReply

		return packet.BroadcastPacket{}, nil
	}

	if searchReply.HopLimit <= 1 {
		return packet.BroadcastPacket{}, errors.New("hop-limit reached")
	}

	gossipPacket := packet.GossipPacket{
		SearchReply: &search.SearchReply{
			Origin:      searchReply.Origin,
			Destination: searchReply.Destination,
			HopLimit:    searchReply.HopLimit - 1,
			Results:     searchReply.Results,
		},
	}

	return gossiper.forward(gossipPacket)
}

func (gossiper *Gossiper) rumorHandlerPeerTLCMessage(tlcMessage packet.TLCMessage, relay net.UDPAddr) (packet.BroadcastPacket, error) {
	gossiper.TLCMessageChannel <- tlcMessage

	broadcastPacket, err := gossiper.randomBroadcast(packet.GossipPacket{
		TLCMessage: &tlcMessage,
	}, []net.UDPAddr{relay})
	if err != nil {
		fmt.Println("ERROR: " + err.Error())
	} else {
		gossiper.BroadcastChannel <- broadcastPacket
	}

	if tlcMessage.Confirmed >= 0 {
		tlcMessage.LogConfirmed()

		return packet.BroadcastPacket{}, nil
	}

	tlcMessage.LogUnconfirmed()

	return packet.BroadcastPacket{}, nil
}

func (gossiper *Gossiper) rumorHandlerPeerTLCAck(tlcAck private.TLCAck) (packet.BroadcastPacket, error) {
	if tlcAck.Destination == gossiper.Name {
		gossiper.TLCAckChannel <- tlcAck

		return packet.BroadcastPacket{}, nil
	}

	if tlcAck.HopLimit <= 1 {
		return packet.BroadcastPacket{}, errors.New("hop-limit reached")
	}

	gossipPacket := packet.GossipPacket{
		Ack: &private.TLCAck{
			Origin:      tlcAck.Origin,
			ID:          tlcAck.ID,
			Text:        tlcAck.Text,
			Destination: tlcAck.Destination,
			HopLimit:    tlcAck.HopLimit - 1,
		},
	}

	return gossiper.forward(gossipPacket)
}

func (gossiper *Gossiper) rumorHandlerPeer(bytes []byte, relay net.UDPAddr) (packet.BroadcastPacket, error) {
	gossipPacket := packet.GossipPacket{}
	if err := protobuf.Decode(bytes, &gossipPacket); err != nil {
		return packet.BroadcastPacket{}, err
	}

	numberFieldsNil := 0
	fields := reflect.ValueOf(&gossipPacket).Elem()
	for i := 0; i < fields.NumField(); i++ {
		if fields.Field(i).IsNil() {
			numberFieldsNil++
		}
	}

	if numberFieldsNil != fields.NumField()-1 {
		return packet.BroadcastPacket{}, errors.New("more or less than a single message inside of GossipPacket")
	}

	if !addr.Contains(append(gossiper.PeersInitial, gossiper.PeersLearned...), relay) {
		gossiper.PeersLearned = append(gossiper.PeersLearned, relay)
	}

	if gossipPacket.Rumor != nil {
		return gossiper.rumorHandlerPeerRumor(*gossipPacket.Rumor, relay)
	} else if gossipPacket.Status != nil {
		return gossiper.rumorHandlerPeerAck(*gossipPacket.Status, relay)
	} else if gossipPacket.Private != nil {
		return gossiper.rumorHandlerPeerPrivate(*gossipPacket.Private)
	} else if gossipPacket.DataRequest != nil {
		return gossiper.rumorHandlerPeerDataRequest(*gossipPacket.DataRequest)
	} else if gossipPacket.DataReply != nil {
		return gossiper.rumorHandlerPeerDataReply(*gossipPacket.DataReply)
	} else if gossipPacket.SearchRequest != nil {
		return gossiper.rumorHandlerPeerSearchRequest(*gossipPacket.SearchRequest)
	} else if gossipPacket.SearchReply != nil {
		return gossiper.rumorHandlerPeerSearchReply(*gossipPacket.SearchReply)
	} else if gossipPacket.TLCMessage != nil {
		return gossiper.rumorHandlerPeerTLCMessage(*gossipPacket.TLCMessage, relay)
	} else if gossipPacket.Ack != nil {
		return gossiper.rumorHandlerPeerTLCAck(*gossipPacket.Ack)
	}

	return packet.BroadcastPacket{}, errors.New("unknown type of packet")
}

func (gossiper *Gossiper) rumorHandler(bytes []byte, relayAddr net.Addr, isClient bool) {
	broadcastPacket := packet.BroadcastPacket{}
	var err error

	if isClient {
		broadcastPacket, err = gossiper.RumorHandlerClient(bytes)
	} else {
		relay, err := net.ResolveUDPAddr(defaults.UDP4, relayAddr.String())
		if err != nil {
			fmt.Println("ERROR: " + err.Error())
			return
		}

		broadcastPacket, err = gossiper.rumorHandlerPeer(bytes, *relay)
	}
	if err != nil {
		fmt.Println("ERROR: " + err.Error())
		return
	}

	if (broadcastPacket.Packet.Rumor != nil && broadcastPacket.Packet.Rumor.Text != "") ||
		(!isClient && (broadcastPacket.Packet.Status != nil ||
			broadcastPacket.Packet.Private != nil ||
			broadcastPacket.Packet.Rumor != nil)) {
		gossiper.LogPeers()
	}

	gossiper.BroadcastChannel <- broadcastPacket
}
