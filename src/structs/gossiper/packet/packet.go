package packet

import (
	"encoding/hex"
	"fmt"
	"github.com/CedMaire/peerster/src/structs/gossiper/blockchain"
	f "github.com/CedMaire/peerster/src/structs/gossiper/file"
	"github.com/CedMaire/peerster/src/structs/gossiper/private"
	"github.com/CedMaire/peerster/src/structs/gossiper/rumor"
	"github.com/CedMaire/peerster/src/structs/gossiper/search"
	"github.com/CedMaire/peerster/src/structs/gossiper/status"
	"github.com/CedMaire/peerster/src/structs/simple/message"
	"net"
	"sort"
	"strconv"
	"strings"
)

type GossipPacket struct {
	Simple        *message.SimpleMessage
	Rumor         *rumor.RumorMessage
	Status        *status.StatusPacket
	Private       *private.PrivateMessage
	DataRequest   *f.DataRequest
	DataReply     *f.DataReply
	SearchRequest *search.SearchRequest
	SearchReply   *search.SearchReply
	TLCMessage    *TLCMessage
	Ack           *private.TLCAck
}

type BroadcastPacket struct {
	Packet       GossipPacket
	Destinations []net.UDPAddr
}

type AckPacket struct {
	From   net.UDPAddr
	Packet status.StatusPacket
}

type TimeoutPacket struct {
	Peer   net.UDPAddr
	Packet rumor.RumorMessage
}

type TLCMessage struct {
	Origin      string
	ID          uint32
	Confirmed   int
	TxBlock     blockchain.BlockPublish
	VectorClock *status.StatusPacket
	Fitness     float32
}

func (tlcMessage TLCMessage) LogUnconfirmed() {
	fmt.Println("UNCONFIRMED GOSSIP origin " + tlcMessage.Origin +
		" ID " + strconv.Itoa(int(tlcMessage.ID)) +
		" file name " + tlcMessage.TxBlock.Transaction.Name +
		" size " + strconv.Itoa(int(tlcMessage.TxBlock.Transaction.Size)) +
		" metahash " + hex.EncodeToString(tlcMessage.TxBlock.Transaction.MetafileHash))
}

func (tlcMessage TLCMessage) LogConfirmed() {
	fmt.Println("CONFIRMED GOSSIP origin " + tlcMessage.Origin +
		" ID " + strconv.Itoa(int(tlcMessage.ID)) +
		" file name " + tlcMessage.TxBlock.Transaction.Name +
		" size " + strconv.Itoa(int(tlcMessage.TxBlock.Transaction.Size)) +
		" metahash " + hex.EncodeToString(tlcMessage.TxBlock.Transaction.MetafileHash))
}

func (tlcMessage TLCMessage) LogReBroadcast(witnesses []string) {
	sort.Strings(witnesses)

	fmt.Println("RE-BROADCAST ID " + strconv.Itoa(int(tlcMessage.ID)) + " WITNESSES " + strings.Join(witnesses, ","))
}

func LogRoundChange(newRound int, messages []TLCMessage) string {
	str := "ADVANCING TO round " + strconv.Itoa(newRound) + " BASED ON CONFIRMED MESSAGES "
	for index, m := range messages {
		str += "origin" + strconv.Itoa(index+1) + " " + m.Origin + " ID" + strconv.Itoa(index+1) + " " + strconv.Itoa(int(m.ID)) + ", "
	}

	finalLog := strings.TrimSuffix(str, ", ")
	fmt.Println(finalLog)

	return finalLog
}
