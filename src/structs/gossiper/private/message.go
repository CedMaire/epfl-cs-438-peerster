package private

import (
	"fmt"
	"strconv"
)

//noinspection GoNameStartsWithPackageName
type PrivateMessage struct {
	Origin      string
	ID          uint32
	Text        string
	Destination string
	HopLimit    uint32
}

type TLCAck PrivateMessage

func (privateMessage PrivateMessage) Log() {
	fmt.Println("PRIVATE origin " + privateMessage.Origin +
		" hop-limit " + strconv.Itoa(int(privateMessage.HopLimit)) +
		" contents " + privateMessage.Text)
}

func (tlcAck TLCAck) Log() {
	fmt.Println("SENDING ACK origin " + tlcAck.Origin + " ID " + strconv.Itoa(int(tlcAck.ID)))
}
