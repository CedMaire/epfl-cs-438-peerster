package file

import (
	"bytes"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"fmt"
	"github.com/CedMaire/peerster/src/defaults"
	cm "github.com/CedMaire/peerster/src/structs/client/message"
	"github.com/CedMaire/peerster/src/structs/gossiper/blockchain"
	"github.com/CedMaire/peerster/src/structs/gossiper/search"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
)

type File struct {
	Name     string
	Origins  []string
	Size     int
	Chunks   [][]byte
	Hashes   [][]byte
	MetaHash []byte
}

type DataRequest struct {
	Origin      string
	Destination string
	HopLimit    uint32
	HashValue   []byte
}

type DataReply struct {
	Origin      string
	Destination string
	HopLimit    uint32
	HashValue   []byte
	Data        []byte
}

type DiscoveredFile struct {
	Names      []string
	ChunkCount uint64
	MetaHash   []byte
	Origins    map[string][]uint64
}

//noinspection GoNameStartsWithPackageName
type FileDatabase struct {
	Data  map[string]File
	Known map[string]DiscoveredFile
}

func (file File) logReconstruct() {
	fmt.Println("RECONSTRUCTED file " + file.Name)
}

func (file File) logDownloadMeta(origin string) {
	fmt.Println("DOWNLOADING metafile of " + file.Name + " from " + origin)
}

func (file File) logDownloadChunk(index int) {
	fmt.Println("DOWNLOADING " + file.Name + " chunk " + strconv.Itoa(index) + " from " + file.Origins[index])
}

// https://stackoverflow.com/a/35179941
func getMetaFile(data []byte) ([][]byte, [][]byte) {
	h := sha256.New()

	var chunks [][]byte
	var hashes [][]byte

	for i := 0; i < len(data); i += defaults.ChunkSize {
		h.Reset()

		end := i + defaults.ChunkSize
		if end > len(data) {
			end = len(data)
		}

		chunks = append(chunks, data[i:end])

		h.Write(data[i:end])
		hashes = append(hashes, h.Sum(nil))
	}

	return chunks, hashes
}

func getMetaHash(hashes [][]byte) []byte {
	h := sha256.New()
	h.Reset()

	for _, hash := range hashes {
		h.Write(hash)
	}

	return h.Sum(nil)
}

func (fileDatabase FileDatabase) GetKeys() []string {
	var keys []string

	for key := range fileDatabase.Data {
		keys = append(keys, key)
	}

	return keys
}

func generateSameOriginArray(origin string, size int) []string {
	var origins []string

	for i := 0; i < size; i++ {
		origins = append(origins, origin)
	}

	return origins
}

func (fileDatabase *FileDatabase) Index(fileName, origin string) (blockchain.BlockPublish, error) {
	baseDir, err := os.Getwd()
	if err != nil {
		return blockchain.BlockPublish{}, err
	}

	fileName = filepath.Join(baseDir, defaults.FileSharedBasePath, filepath.Base(fileName))
	data, err := ioutil.ReadFile(fileName)
	if err != nil {
		return blockchain.BlockPublish{}, err
	}

	size := len(data)
	chunks, hashes := getMetaFile(data)
	metaHash := getMetaHash(hashes)

	fileDatabase.Data[base64.StdEncoding.EncodeToString(metaHash)] = File{
		Name:     filepath.Base(fileName),
		Origins:  generateSameOriginArray(origin, len(hashes)),
		Size:     size,
		Chunks:   chunks,
		Hashes:   hashes,
		MetaHash: metaHash,
	}

	return blockchain.BlockPublish{
		PrevHash: [sha256.Size]byte{},
		Transaction: blockchain.TxPublish{
			Name:         filepath.Base(fileName),
			Size:         int64(size),
			MetafileHash: metaHash,
		},
	}, nil
}

func (fileDatabase *FileDatabase) AddNewFileToDownload(message cm.Message, origin string) (DataRequest, error) {
	if len(*message.Request) != sha256.Size {
		return DataRequest{}, errors.New("incorrect meta-hash size")
	}

	destination := *message.Destination
	if destination == defaults.ClientDestination {
		if discoveredFile, ok := fileDatabase.Known[base64.StdEncoding.EncodeToString(*message.Request)]; ok {
			if discoveredFile.IsComplete() {
				if _, ok := fileDatabase.Data[base64.StdEncoding.EncodeToString(*message.Request)]; !ok {
					err, newFile := discoveredFile.toFile(*message.File)
					if err != nil {
						return DataRequest{}, err
					}

					fileDatabase.Data[base64.StdEncoding.EncodeToString(*message.Request)] = newFile

					return DataRequest{
						Origin:      origin,
						Destination: newFile.Origins[0],
						HopLimit:    defaults.FileHopeLimit - 1,
						HashValue:   *message.Request,
					}, nil
				} else {
					return DataRequest{}, errors.New("file already downloading/downloaded")
				}
			} else {
				return DataRequest{}, errors.New("discovered file not ready to be downloaded")
			}
		}

		return DataRequest{}, errors.New("discovered file unknown")
	}

	if file, ok := fileDatabase.Data[base64.StdEncoding.EncodeToString(*message.Request)]; !ok {
		fileDatabase.Data[base64.StdEncoding.EncodeToString(*message.Request)] = File{
			Name:     *message.File,
			Origins:  generateSameOriginArray(destination, 1),
			Size:     -1,
			Chunks:   [][]byte{},
			Hashes:   [][]byte{},
			MetaHash: *message.Request,
		}

		return DataRequest{
			Origin:      origin,
			Destination: destination,
			HopLimit:    defaults.FileHopeLimit - 1,
			HashValue:   *message.Request,
		}, nil
	} else if len(file.Hashes) > 0 && len(file.Hashes) != len(file.Chunks) {
		return DataRequest{
			Origin:      origin,
			Destination: destination,
			HopLimit:    defaults.FileHopeLimit - 1,
			HashValue:   file.Hashes[len(file.Chunks)],
		}, nil
	} else if len(file.Hashes) > 0 && len(file.Hashes) == len(file.Chunks) {
		fileDatabase.WriteToDisc()

		return DataRequest{}, errors.New("file already complete")
	} else {
		return DataRequest{
			Origin:      origin,
			Destination: destination,
			HopLimit:    defaults.FileHopeLimit - 1,
			HashValue:   file.MetaHash,
		}, nil
	}
}

func (fileDatabase *FileDatabase) GetNextDataRequests(origin string) []DataRequest {
	var dataRequests []DataRequest

	for _, file := range fileDatabase.Data {
		if len(file.Hashes) == 0 {
			dataRequests = append(dataRequests, DataRequest{
				Origin:      origin,
				Destination: file.Origins[0],
				HopLimit:    defaults.FileHopeLimit - 1,
				HashValue:   file.MetaHash,
			})
		} else if len(file.Chunks) < len(file.Hashes) {
			dataRequests = append(dataRequests, DataRequest{
				Origin:      origin,
				Destination: file.Origins[len(file.Chunks)],
				HopLimit:    defaults.FileHopeLimit - 1,
				HashValue:   file.Hashes[len(file.Chunks)],
			})
		}
	}

	return dataRequests
}

func splitAndVerifyHashes(metaHash, hashes []byte) ([][]byte, error) {
	if len(hashes)%sha256.Size != 0 {
		return nil, errors.New("meta-hash data should be a multiple of " + strconv.Itoa(sha256.Size))
	}

	h := sha256.New()
	h.Write(hashes)
	if !bytes.Equal(h.Sum(nil), metaHash) {
		return nil, errors.New("hash of meta-hash data is different from the expected meta-hash")
	}

	var chunks [][]byte
	for i := 0; i < len(hashes); i += sha256.Size {
		chunks = append(chunks, hashes[i:i+sha256.Size])
	}

	return chunks, nil
}

func (fileDatabase *FileDatabase) UpdateFile(dataReply DataReply) (bool, DataRequest) {
	if fileStruct0, ok := fileDatabase.Data[base64.StdEncoding.EncodeToString(dataReply.HashValue)]; ok {
		if len(fileStruct0.Hashes) > 0 && len(fileStruct0.Hashes) == len(fileStruct0.Chunks) {
			fileDatabase.WriteToDisc()
			return true, DataRequest{}
		}

		chunks, err := splitAndVerifyHashes(dataReply.HashValue, dataReply.Data)
		if err == nil {
			fileStruct0.logDownloadMeta(dataReply.Origin)

			if len(fileStruct0.Origins) == 1 && len(chunks) > 1 {
				fileDatabase.Data[base64.StdEncoding.EncodeToString(dataReply.HashValue)] = File{
					Name:     fileStruct0.Name,
					Origins:  generateSameOriginArray(fileStruct0.Origins[0], len(chunks)),
					Size:     fileStruct0.Size,
					Chunks:   fileStruct0.Chunks,
					Hashes:   chunks,
					MetaHash: fileStruct0.MetaHash,
				}
			} else {
				fileDatabase.Data[base64.StdEncoding.EncodeToString(dataReply.HashValue)] = File{
					Name:     fileStruct0.Name,
					Origins:  fileStruct0.Origins,
					Size:     fileStruct0.Size,
					Chunks:   fileStruct0.Chunks,
					Hashes:   chunks,
					MetaHash: fileStruct0.MetaHash,
				}
			}
		}

		if len(chunks) > 0 {
			return false, DataRequest{
				Origin:      dataReply.Destination,
				Destination: fileStruct0.Origins[len(fileStruct0.Chunks)],
				HopLimit:    defaults.FileHopeLimit - 1,
				HashValue:   chunks[len(fileStruct0.Chunks)],
			}
		} else {
			return false, DataRequest{
				Origin:      dataReply.Destination,
				Destination: fileStruct0.Origins[0],
				HopLimit:    defaults.FileHopeLimit - 1,
				HashValue:   fileStruct0.MetaHash,
			}
		}
	}

	var fileBase64 = ""
	var index = -1
	for fileString, fileStruct1 := range fileDatabase.Data {
		for i, chunkHash := range fileStruct1.Hashes {
			if bytes.Equal(dataReply.HashValue, chunkHash) {
				if i == len(fileStruct1.Chunks) {
					fileBase64 = fileString
					index = i
				}
			}
		}
	}
	if fileBase64 != "" && index != -1 {
		fileStruct1 := fileDatabase.Data[fileBase64]
		chunkHash := fileStruct1.Hashes[index]

		h := sha256.New()
		h.Write(dataReply.Data)

		if bytes.Equal(h.Sum(nil), chunkHash) {
			fileStruct1.logDownloadChunk(index)
			fileDatabase.Data[base64.StdEncoding.EncodeToString(fileStruct1.MetaHash)] = File{
				Name:     fileStruct1.Name,
				Origins:  fileStruct1.Origins,
				Size:     fileStruct1.Size,
				Chunks:   append(fileStruct1.Chunks, dataReply.Data),
				Hashes:   fileStruct1.Hashes,
				MetaHash: fileStruct1.MetaHash,
			}
			fileStruct1.Chunks = append(fileStruct1.Chunks, dataReply.Data)
		}

		if len(fileStruct1.Hashes) > 0 && len(fileStruct1.Hashes) == len(fileStruct1.Chunks) {
			fileDatabase.WriteToDisc()
			return true, DataRequest{}
		}

		return false, DataRequest{
			Origin:      dataReply.Destination,
			Destination: dataReply.Origin,
			HopLimit:    defaults.FileHopeLimit - 1,
			HashValue:   fileStruct1.Hashes[len(fileStruct1.Chunks)],
		}
	}

	return true, DataRequest{}
}

func flatten(data [][]byte) []byte {
	var result []byte

	for _, row := range data {
		result = append(result, row...)
	}

	return result
}

func (fileDatabase *FileDatabase) Reply(dataRequest DataRequest) DataReply {
	if fileStruct0, ok := fileDatabase.Data[base64.StdEncoding.EncodeToString(dataRequest.HashValue)]; ok {
		if len(fileStruct0.Hashes) > 0 {
			return DataReply{
				Origin:      dataRequest.Destination,
				Destination: dataRequest.Origin,
				HopLimit:    defaults.FileHopeLimit - 1,
				HashValue:   dataRequest.HashValue,
				Data:        flatten(fileStruct0.Hashes),
			}
		}
	}

	for _, fileStruct1 := range fileDatabase.Data {
		for index, chunkHash := range fileStruct1.Hashes {
			if bytes.Equal(dataRequest.HashValue, chunkHash) {
				if len(fileStruct1.Chunks) > index {
					return DataReply{
						Origin:      dataRequest.Destination,
						Destination: dataRequest.Origin,
						HopLimit:    defaults.FileHopeLimit - 1,
						HashValue:   dataRequest.HashValue,
						Data:        fileStruct1.Chunks[index],
					}
				}
			}
		}
	}

	return DataReply{
		Origin:      dataRequest.Destination,
		Destination: dataRequest.Origin,
		HopLimit:    defaults.FileHopeLimit - 1,
		HashValue:   dataRequest.HashValue,
		Data:        []byte{},
	}
}

func (fileDatabase *FileDatabase) WriteToDisc() {
	for fileHash, file := range fileDatabase.Data {
		if file.Size < 0 &&
			len(file.Hashes) > 0 &&
			len(file.Hashes) == len(file.Chunks) {
			baseDir, err := os.Getwd()
			if err != nil {
				fmt.Println("ERROR: " + err.Error())
				return
			}

			fileName := filepath.Join(baseDir, defaults.FileDownloadedBasePath, filepath.Base(file.Name))
			err = ioutil.WriteFile(fileName, flatten(file.Chunks), 0666)
			if err != nil {
				fmt.Println("ERROR: " + err.Error())
				return
			}

			file.logReconstruct()

			fileDatabase.Data[fileHash] = File{
				Name:     file.Name,
				Origins:  file.Origins,
				Size:     len(flatten(file.Chunks)),
				Chunks:   file.Chunks,
				Hashes:   file.Hashes,
				MetaHash: file.MetaHash,
			}
		}
	}
}

func (file File) matches(keywords []string) bool {
	for _, keyword := range keywords {
		if strings.Contains(file.Name, keyword) {
			return true
		}
	}

	return false
}

func matches(names, keywords []string) bool {
	for _, keyword := range keywords {
		for _, name := range names {
			if strings.Contains(name, keyword) {
				return true
			}
		}
	}

	return false
}

func getRange(n int) []uint64 {
	var newRange []uint64

	for i := 1; i <= n; i++ {
		newRange = append(newRange, uint64(i))
	}

	return newRange
}

func (fileDatabase FileDatabase) GetSearchReply(searchRequest search.SearchRequest, newOrigin string) (bool, search.SearchReply) {
	var searchResults []*search.SearchResult

	for _, file := range fileDatabase.Data {
		if file.matches(searchRequest.Keywords) {
			searchResults = append(searchResults, &search.SearchResult{
				Filename:     file.Name,
				MetafileHash: file.MetaHash,
				ChunkMap:     getRange(len(file.Chunks)),
				ChunkCount:   uint64(len(file.Hashes)),
			})
		}
	}

	return len(searchResults) > 0, search.SearchReply{
		Origin:      newOrigin,
		Destination: searchRequest.Origin,
		HopLimit:    defaults.FileHopeLimit,
		Results:     searchResults,
	}
}

func intArrayContains(arr []int, x int) bool {
	for _, e := range arr {
		if e == x {
			return true
		}
	}

	return false
}

func uint64ArrayContains(arr []uint64, x uint64) bool {
	for _, e := range arr {
		if e == x {
			return true
		}
	}

	return false
}

func mergeUniqueOrdered(origins map[string][]uint64) []uint64 {
	var resultInt []int

	for _, chunks := range origins {
		for _, chunk := range chunks {
			if !intArrayContains(resultInt, int(chunk)) {
				resultInt = append(resultInt, int(chunk))
			}
		}
	}

	sort.Ints(resultInt)

	var result []uint64
	for _, e := range resultInt {
		result = append(result, uint64(e))
	}

	return result
}

func (discoveredFile DiscoveredFile) IsComplete() bool {
	return uint64(len(mergeUniqueOrdered(discoveredFile.Origins))) == discoveredFile.ChunkCount
}

func (discoveredFile DiscoveredFile) toFile(name string) (error, File) {
	if !discoveredFile.IsComplete() {
		return errors.New("file not complete"), File{}
	}

	var origins []string
	for i := 1; uint64(i) <= discoveredFile.ChunkCount; i++ {
		for origin, chunks := range discoveredFile.Origins {
			if uint64ArrayContains(chunks, uint64(i)) {
				origins = append(origins, origin)
				break
			}
		}
	}

	return nil, File{
		Name:     name,
		Origins:  origins,
		Size:     -1,
		Chunks:   [][]byte{},
		Hashes:   [][]byte{},
		MetaHash: discoveredFile.MetaHash,
	}
}

func (fileDatabase *FileDatabase) UpdateDiscoveredFile(searchReply search.SearchReply) {
	for _, result := range searchReply.Results {
		search.LogMatchFound(result.Filename, searchReply.Origin, result.MetafileHash, result.ChunkMap)

		if discoveredFile, ok := fileDatabase.Known[base64.StdEncoding.EncodeToString(result.MetafileHash)]; ok {
			var newNames []string
			newOrigins := make(map[string][]uint64)

			nameAlreadyKnown := false
			for _, name := range discoveredFile.Names {
				newNames = append(newNames, name)
				if name == result.Filename {
					nameAlreadyKnown = true
				}
			}
			if !nameAlreadyKnown {
				newNames = append(newNames, result.Filename)
			}

			for key, value := range discoveredFile.Origins {
				newOrigins[key] = value
			}
			newOrigins[result.Filename] = result.ChunkMap

			fileDatabase.Known[base64.StdEncoding.EncodeToString(result.MetafileHash)] = DiscoveredFile{
				Names:      newNames,
				ChunkCount: discoveredFile.ChunkCount,
				MetaHash:   discoveredFile.MetaHash,
				Origins:    newOrigins,
			}
		} else {
			fileDatabase.Known[base64.StdEncoding.EncodeToString(result.MetafileHash)] = DiscoveredFile{
				Names:      []string{result.Filename},
				ChunkCount: result.ChunkCount,
				MetaHash:   result.MetafileHash,
				Origins: map[string][]uint64{
					searchReply.Origin: result.ChunkMap,
				},
			}
		}
	}
}

func (fileDatabase *FileDatabase) SearchMatches(keywords []string) uint {
	numberMatches := 0

	for _, discoveredFile := range fileDatabase.Known {
		if matches(discoveredFile.Names, keywords) && discoveredFile.IsComplete() {
			numberMatches += 1
		}
	}

	return uint(numberMatches)
}
