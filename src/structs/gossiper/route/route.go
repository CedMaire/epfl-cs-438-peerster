package route

import (
	"fmt"
	"github.com/CedMaire/peerster/src/structs/gossiper/rumor"
	"net"
)

//noinspection GoNameStartsWithPackageName
type RouteEntry struct {
	ID   uint32
	Addr net.UDPAddr
}

type RoutingTable struct {
	Data map[string]RouteEntry
}

func (routeEntry RouteEntry) log(origin string) {
	fmt.Println("DSDV " + origin + " " + routeEntry.Addr.String())
}

func (routingTable *RoutingTable) Update(rumorMessage rumor.RumorMessage, relay net.UDPAddr) {
	if entry, ok := routingTable.Data[rumorMessage.Origin]; (ok && rumorMessage.ID > entry.ID) || !ok {
		routingTable.Data[rumorMessage.Origin] = RouteEntry{
			ID:   rumorMessage.ID,
			Addr: relay,
		}

		if rumorMessage.Text != "" {
			routingTable.Data[rumorMessage.Origin].log(rumorMessage.Origin)
		}
	}
}
