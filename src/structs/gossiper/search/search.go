package search

import (
	"encoding/hex"
	"fmt"
	"github.com/CedMaire/peerster/src/defaults"
	"math/rand"
	"net"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"time"
)

type BudAddr struct {
	Budget  uint64
	Address net.UDPAddr
}

//noinspection GoNameStartsWithPackageName
type SearchResult struct {
	Filename     string
	MetafileHash []byte
	ChunkMap     []uint64
	ChunkCount   uint64
}

//noinspection GoNameStartsWithPackageName
type SearchRequest struct {
	Origin   string
	Budget   uint64
	Keywords []string
}

//noinspection GoNameStartsWithPackageName
type SearchReply struct {
	Origin      string
	Destination string
	HopLimit    uint32
	Results     []*SearchResult
}

func LogSearchFinished(nMatches uint) {
	if nMatches >= defaults.FileSearchMinMatches {
		fmt.Println("SEARCH FINISHED")
	} else {
		fmt.Println("SEARCH FAILED, NOT ENOUGH MATCHES")
	}
}

func LogMatchFound(filename, origin string, metafileHash []byte, chunkMap []uint64) {
	var chunks []string
	for _, chunk := range chunkMap {
		chunks = append(chunks, strconv.Itoa(int(chunk)))
	}

	fmt.Println("FOUND match " + filename + " at " + origin + " metafile=" + hex.EncodeToString(metafileHash) +
		" chunks=" + strings.Join(chunks, ","))
}

func sameKeywords(kws0, kws1 []string) bool {
	sort.Strings(kws0)
	sort.Strings(kws1)

	return reflect.DeepEqual(kws0, kws1)
}

func (searchRequest SearchRequest) Equals(other SearchRequest) bool {
	return searchRequest.Origin == other.Origin && sameKeywords(searchRequest.Keywords, other.Keywords)
}

func filterNoBudget(distribution []BudAddr) []BudAddr {
	var newDistribution []BudAddr

	for _, budAddr := range distribution {
		if budAddr.Budget > 0 {
			newDistribution = append(newDistribution, budAddr)
		}
	}

	return newDistribution
}

// Shuffle functionality from: https://yourbasic.org/golang/shuffle-slice-array/
func GetBudgetedDistribution(peers []net.UDPAddr, budget uint64) []BudAddr {
	if len(peers) == 0 {
		return []BudAddr{}
	}

	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(peers), func(i, j int) { peers[i], peers[j] = peers[j], peers[i] })

	baseBudget := budget / uint64(len(peers))
	extraBudget := budget % uint64(len(peers))

	var distribution []BudAddr

	for index, peer := range peers {
		if uint64(index) < extraBudget {
			distribution = append(distribution, BudAddr{
				Budget:  baseBudget + 1,
				Address: peer,
			})
		} else {
			distribution = append(distribution, BudAddr{
				Budget:  baseBudget,
				Address: peer,
			})
		}
	}

	return filterNoBudget(distribution)
}
