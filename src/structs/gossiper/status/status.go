package status

import (
	"fmt"
	"net"
	"strconv"
)

type PeerStatus struct {
	Identifier string
	NextID     uint32
}

//noinspection GoNameStartsWithPackageName
type StatusPacket struct {
	Want []PeerStatus
}

func (statusPacket StatusPacket) LogPacket(relay net.UDPAddr) {
	var toPrint = "STATUS from " + relay.String()
	for _, status := range statusPacket.Want {
		toPrint += " peer " + status.Identifier + " nextID " + strconv.Itoa(int(status.NextID))
	}

	fmt.Println(toPrint)
}

func (statusPacket StatusPacket) ToMap() map[string]uint32 {
	statusMap := make(map[string]uint32)

	for _, peerStatus := range statusPacket.Want {
		statusMap[peerStatus.Identifier] = peerStatus.NextID
	}

	return statusMap
}
