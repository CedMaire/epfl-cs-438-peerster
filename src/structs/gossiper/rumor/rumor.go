package rumor

import (
	"fmt"
	"net"
	"strconv"
)

//noinspection GoNameStartsWithPackageName
type RumorMessage struct {
	Origin string
	ID     uint32
	Text   string
}

func (rumorMessage RumorMessage) Log(relayPeer net.UDPAddr) {
	fmt.Println("RUMOR origin " + rumorMessage.Origin +
		" from " + relayPeer.String() +
		" ID " + strconv.Itoa(int(rumorMessage.ID)) +
		" contents " + rumorMessage.Text)
}

func (rumorMessage RumorMessage) Equals(m RumorMessage) bool {
	return rumorMessage.Origin == m.Origin && rumorMessage.ID == m.ID
}

func (rumorMessage RumorMessage) Contained(list []RumorMessage) bool {
	for _, message := range list {
		if rumorMessage.Equals(message) {
			return true
		}
	}

	return false
}

func (rumorMessage RumorMessage) AddTo(list []RumorMessage) []RumorMessage {
	if !rumorMessage.Contained(list) {
		return append(list, rumorMessage)
	}

	return list
}

func (rumorMessage RumorMessage) RemoveFrom(list []RumorMessage) []RumorMessage {
	var newList []RumorMessage

	for _, message := range list {
		if message.Origin == rumorMessage.Origin && message.ID > rumorMessage.ID {
			newList = append(newList, message)
		}
	}

	return newList
}
