package addr

import (
	"fmt"
	"github.com/CedMaire/peerster/src/defaults"
	cm "github.com/CedMaire/peerster/src/structs/client/message"
	"github.com/dedis/protobuf"
	"net"
	"time"
)

type Packet struct {
	Relay net.Addr
	Bytes []byte
}

func ReadSocket(connection net.UDPConn, packetChan chan Packet) {
	for {
		bytes := make([]byte, defaults.BufferSize)
		length, relay, err := connection.ReadFrom(bytes)
		if err != nil {
			fmt.Println("ERROR: " + err.Error())
			continue
		}

		packetChan <- Packet{
			Relay: relay,
			Bytes: bytes[:length],
		}
	}
}

func sendRoute(packetChan chan Packet) {
	m := cm.Message{
		Text: "",
	}

	packetBytes, err := protobuf.Encode(&m)
	if err != nil {
		fmt.Println("ERROR: " + err.Error())
		return
	}

	packetChan <- Packet{
		Relay: nil,
		Bytes: packetBytes,
	}
}

func Route(interval int, packetChan chan Packet) {
	sendRoute(packetChan)

	routeTimer := time.NewTicker(time.Duration(interval) * time.Second)
	for {
		select {
		case _ = <-routeTimer.C:
			sendRoute(packetChan)
		}
	}
}

func Equals(net0, net1 net.UDPAddr) bool {
	return net0.IP.String() == net1.IP.String() && net0.Port == net1.Port
}

func Contains(list []net.UDPAddr, element net.UDPAddr) bool {
	for _, address := range list {
		if Equals(element, address) {
			return true
		}
	}

	return false
}

func Add(list []net.UDPAddr, element net.UDPAddr) []net.UDPAddr {
	if !Contains(list, element) {
		return append(list, element)
	}

	return list
}

func Remove(list []net.UDPAddr, element net.UDPAddr) []net.UDPAddr {
	var newList []net.UDPAddr

	for _, peer := range list {
		if !Equals(element, peer) {
			newList = append(newList, peer)
		}
	}

	return newList
}
