package message

import "fmt"

type SimpleMessage struct {
	OriginalName     string
	RelayPeerAddress string
	Contents         string
}

func (simpleMessage SimpleMessage) Log() {
	fmt.Println("SIMPLE MESSAGE origin " + simpleMessage.OriginalName +
		" from " + simpleMessage.RelayPeerAddress +
		" contents " + simpleMessage.Contents)
}

func (simpleMessage SimpleMessage) Equals(m SimpleMessage) bool {
	return simpleMessage.OriginalName == m.OriginalName && simpleMessage.Contents == m.Contents
}

func (simpleMessage SimpleMessage) Contained(list []SimpleMessage) bool {
	for _, message := range list {
		if simpleMessage.Equals(message) {
			return true
		}
	}

	return false
}
