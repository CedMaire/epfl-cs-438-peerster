package client

import (
	"fmt"
	cm "github.com/CedMaire/peerster/src/structs/client/message"
	"github.com/dedis/protobuf"
	"net"
)

type Client struct {
	Name             string
	ClientAddress    net.UDPAddr
	ClientConnection net.UDPConn
	Message          string
	Destination      string
	File             string
	Request          []byte
	Budget           uint64
	Keywords         []string
}

func (client Client) Send() {
	m := cm.Message{
		Text:        client.Message,
		Destination: &client.Destination,
		File:        &client.File,
		Request:     &client.Request,
		Budget:      &client.Budget,
		Keywords:    &client.Keywords,
	}

	packetBytes, err := protobuf.Encode(&m)
	if err != nil {
		fmt.Println("ERROR: " + err.Error())
		return
	}

	_, err = client.ClientConnection.WriteToUDP(packetBytes, &client.ClientAddress)
	if err != nil {
		fmt.Println("ERROR: " + err.Error())
		return
	}
}
