package message

import (
	"fmt"
	"github.com/CedMaire/peerster/src/defaults"
)

type Message struct {
	Text        string
	Destination *string
	File        *string
	Request     *[]byte
	Budget      *uint64
	Keywords    *[]string
}

func (message Message) IsFileRequest() bool {
	return (message.Destination != nil) &&
		(message.File != nil && *message.File != defaults.FilePath) &&
		(message.Request != nil && len(*message.Request) > 0)
}

func (message Message) IsPrivate() bool {
	return message.Destination != nil && *message.Destination != defaults.ClientDestination
}

func (message Message) IsFileIndex() bool {
	return message.File != nil && *message.File != defaults.FilePath
}

func (message Message) IsFileSearch() bool {
	return message.Budget != nil && (message.Keywords != nil && len(*message.Keywords) > 0)
}

func (message Message) Log() {
	toPrint := "CLIENT MESSAGE " + message.Text
	if message.IsPrivate() {
		toPrint = toPrint + " dest " + *message.Destination
	}

	fmt.Println(toPrint)
}
