package web

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/CedMaire/peerster/src/defaults"
	"github.com/CedMaire/peerster/src/structs/addr"
	"github.com/CedMaire/peerster/src/structs/client/message"
	g "github.com/CedMaire/peerster/src/structs/gossiper"
	"github.com/dedis/protobuf"
	"math"
	"net"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

func forwardMessageGUI(gossiper *g.Gossiper, text, destination, file, request, budgetString, keywordsString string) {
	var requestBytes []byte
	var err error

	if request != defaults.FileRequest {
		requestBytes, err = hex.DecodeString(request)
		if err != nil {
			fmt.Println("ERROR: " + err.Error())
			return
		}
	}

	// Inspired from: https://stackoverflow.com/a/55604797
	isNum, err := regexp.MatchString(`^\d+(\.\d*)?$`, strings.TrimSpace(budgetString))
	var budget uint64
	if isNum {
		myBud, err := strconv.Atoi(budgetString)
		if err == nil {
			budget = uint64(math.Max(math.Min(defaults.FileSearchBudgetMax, float64(myBud)), defaults.FileSearchBudgetMin))
		} else {
			budget = 2
		}
	} else {
		budget = 2
	}

	keywords := strings.Split(keywordsString, ",")
	if len(keywords) > 0 && keywords[0] == defaults.FileSearchKeywords {
		keywords = []string{}
	}

	m := message.Message{
		Text:        text,
		Destination: &destination,
		File:        &file,
		Request:     &requestBytes,
		Budget:      &budget,
		Keywords:    &keywords,
	}

	bytes, err := protobuf.Encode(&m)
	if err != nil {
		fmt.Println("ERROR: " + err.Error())
		return
	}

	broadcastPacket, err := gossiper.RumorHandlerClient(bytes)
	if err != nil {
		fmt.Println("ERROR: " + err.Error())
		return
	}

	if *m.File == defaults.FilePath {
		gossiper.LogPeers()
	}
	gossiper.BroadcastChannel <- broadcastPacket
}

func addNewNode(gossiper *g.Gossiper, address string) {
	relayPeerAddress, err := net.ResolveUDPAddr(defaults.UDP4, address)
	if err != nil {
		fmt.Println("ERROR: " + err.Error())
	} else if !addr.Contains(append(gossiper.PeersInitial, gossiper.PeersLearned...), *relayPeerAddress) {
		gossiper.PeersLearned = append(gossiper.PeersLearned, *relayPeerAddress)
	}
}

func getGUIUpdate(gossiper *g.Gossiper) []byte {
	jsonMap := make(map[string]string)

	jsonMap["peer_id"] = gossiper.Name
	jsonMap["peer_round"] = strconv.Itoa(gossiper.Round)

	nodesJSON, err := json.Marshal(append(gossiper.PeersInitial, gossiper.PeersLearned...))
	if err != nil {
		jsonMap["nodes"] = "[]"
	} else {
		jsonMap["nodes"] = string(nodesJSON)
	}
	messagesJSON, err := json.Marshal(gossiper.Database.Data)
	if err != nil {
		jsonMap["messages"] = "[]"
	} else {
		jsonMap["messages"] = string(messagesJSON)
	}
	routeJSON, err := json.Marshal(gossiper.RoutingTable.Data)
	if err != nil {
		jsonMap["routes"] = "[]"
	} else {
		jsonMap["routes"] = string(routeJSON)
	}
	privateJSON, err := json.Marshal(gossiper.PrivateDB.Data)
	if err != nil {
		jsonMap["private"] = "[]"
	} else {
		jsonMap["private"] = string(privateJSON)
	}
	historyJSON, err := json.Marshal(gossiper.RoundChangeHistory)
	if err != nil {
		jsonMap["peer_round_history"] = "[]"
	} else {
		jsonMap["peer_round_history"] = string(historyJSON)
	}
	searchRepliesJSON, err := json.Marshal(gossiper.SearchReplies)
	if err != nil {
		jsonMap["search_replies"] = "[]"
	} else {
		jsonMap["search_replies"] = string(searchRepliesJSON)
	}

	mapJSON, err := json.Marshal(jsonMap)
	if err != nil {
		return make([]byte, 0)
	}

	return mapJSON
}

func StartServer(gossiper *g.Gossiper) {
	http.Handle("/", http.FileServer(http.Dir("src/web/")))
	http.HandleFunc("/update", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case "GET":
			updateJSON := getGUIUpdate(gossiper)
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			_, _ = w.Write(updateJSON)
		case "POST":
			if err := r.ParseForm(); err != nil {
				fmt.Println("ERROR: " + err.Error())
				return
			}

			if r.FormValue("type") == "node" {
				addNewNode(gossiper, r.FormValue("value"))
			} else if r.FormValue("type") == "message" {
				forwardMessageGUI(gossiper,
					r.FormValue("value"),
					r.FormValue("destination"),
					r.FormValue("file"),
					r.FormValue("request"),
					r.FormValue("budget"),
					r.FormValue("keywords"))
			} else {
				fmt.Println("ERROR: unknown data from GUI")
			}
		default:
			fmt.Println("ERROR: only GET/POST requests supported")
		}
	})

	err := http.ListenAndServe(defaults.LocalHost+":"+gossiper.GuiPort, nil)
	if err != nil {
		fmt.Println("ERROR: " + err.Error())
	}
}
